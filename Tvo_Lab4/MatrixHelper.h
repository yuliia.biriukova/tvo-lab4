#pragma once

class MatrixHelper {
public:
    static int** generateMatrix(int size);
    static int sequentialMaxRowSum(int** matrix, int size);
    static int parallelMaxRowSum(int** matrix, int size);
    static void deleteMatrix(int** matrix, int size);
};