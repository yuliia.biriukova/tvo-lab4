﻿#include "MatrixHelper.h"
#include <iostream>
#include <omp.h>

void compareMethods(int matrixSize) {
    // Generate the matrix
    int** matrix = MatrixHelper::generateMatrix(matrixSize);

    // Sequential algorithm
    double startSeq = omp_get_wtime();
    int seqResult = MatrixHelper::sequentialMaxRowSum(matrix, matrixSize);
    double endSeq = omp_get_wtime();
    double seqDuration = endSeq - startSeq;

    std::cout << "Sequential Max Row Sum: " << seqResult << std::endl;
    std::cout << "Sequential Execution Time: " << seqDuration << " seconds\n" << std::endl;

    // OpenMP algorithm
    double startOMP = omp_get_wtime();
    int ompResult = MatrixHelper::parallelMaxRowSum(matrix, matrixSize);
    double endOMP = omp_get_wtime();
    double ompDuration = endOMP - startOMP;

    std::cout << "OpenMP Max Row Sum: " << ompResult << std::endl;
    std::cout << "OpenMP Execution Time: " << ompDuration << " seconds" << std::endl;

    // Clean up memory
    MatrixHelper::deleteMatrix(matrix, matrixSize);
}

int main() {
    std::cout << "\n----- Time comparison for matrixSize = 500 -----\n" << std::endl;
    compareMethods(500);

    std::cout << "\n----- Time comparison for matrixSize = 1000 -----\n" << std::endl;
    compareMethods(1000);

    std::cout << "\n----- Time comparison for matrixSize = 5000 -----\n" << std::endl;
    compareMethods(5000);

    std::cout << "\n----- Time comparison for matrixSize = 10000 -----\n" << std::endl;
    compareMethods(10000);

    std::cout << "\n----- Time comparison for matrixSize = 15000 -----\n" << std::endl;
    compareMethods(15000);

    std::cout << "\n----- Time comparison for matrixSize = 20000 -----\n" << std::endl;
    compareMethods(20000);

    std::cout << "\n----- Time comparison for matrixSize = 25000 -----\n" << std::endl;
    compareMethods(25000);

    std::cout << "\n----- Time comparison for matrixSize = 30000 -----\n" << std::endl;
    compareMethods(30000);

    return 0;
}