#include "MatrixHelper.h"
#include <cstdlib>
#include <iostream>
#include <omp.h>

// Function to generate a square matrix with random values
int** MatrixHelper::generateMatrix(int size) {
    // Allocate memory for the matrix
    int** matrix = new int* [size];
    for (int i = 0; i < size; ++i) {
        matrix[i] = new int[size];
    }

    // Generate the matrix
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            matrix[i][j] = rand() % 100;  // Random values between 0 and 99
        }
    }

    return matrix;
}

// Function to generate a square matrix with random values
int MatrixHelper::sequentialMaxRowSum(int** matrix, int size) {
    int maxSum = 0;

    std::cout << "Number of threads used: " << omp_get_num_threads() << std::endl;

    for (int i = 0; i < size; ++i) {
        int rowSum = 0;
        for (int j = 0; j < size; ++j) {
            rowSum += matrix[i][j];
        }
        maxSum = std::max(maxSum, rowSum);
    }

    return maxSum;
}

// OpenMP algorithm to find the maximum sum of matrix rows
int MatrixHelper::parallelMaxRowSum(int** matrix, int size) {
    int maxSum = 0;

    #pragma omp parallel 
    {
        #pragma omp master
        {
            std::cout << "Number of threads used: " << omp_get_num_threads() << std::endl;
        }

        #pragma omp for reduction(max : maxSum)
        for (int i = 0; i < size; ++i) {
            int rowSum = 0;
            for (int j = 0; j < size; ++j) {
                rowSum += matrix[i][j];
            }
            maxSum = std::max(maxSum, rowSum);
        }
    }
   
    return maxSum;
}

// Function to deallocate memory for the matrix
void MatrixHelper::deleteMatrix(int** matrix, int size) {
    for (int i = 0; i < size; ++i) {
        delete[] matrix[i];
    }
    delete[] matrix;
}